use rocket::http::Status;
use rocket::Request;
use rocket::response::{self, Responder, Response};

pub struct CacheForever<T>(pub T);

impl<'r, 'o: 'r, T: Responder<'r, 'o> + Send> Responder<'r, 'o> for CacheForever<T> {
    fn respond_to(self, req: &'r Request<'_>) -> response::Result<'o> {
        let etag = concat!("\"", include_str!(concat!(env!("OUT_DIR"), "/head")), "\"");
        for if_none_match in req.headers().get("if-none-match") {
            if if_none_match == etag {
                return Err(Status::NotModified);
            }
        }
        let mut build = Response::build();
        build.merge(self.0.respond_to(req)?);
        build.raw_header("cache-control", "public, max-age=3600");
        build.raw_header("etag", etag);
        build.ok()
    }
}
