use std::collections::HashMap;

use askama::Template;

use crate::survey::SurveyData;

#[derive(Template)]
#[template(path = "index.html")]
pub struct Index;

#[derive(Template)]
#[template(path = "voteform.html")]
pub struct VoteForm {
    pub title: String,
    pub data: HashMap<String, SurveyData>,
    pub url: String,
    pub cookie_set: bool,
    pub is_closed: bool,
    pub has_password: bool,
}

#[derive(Template)]
#[template(path = "results.html")]
pub struct Results {
    pub title: String,
    pub data: Vec<(String, SurveyData)>,
    pub url: String,
    pub is_closed: bool,
    pub has_password: bool,
    pub bad_password: bool,
}
