use std::collections::HashMap;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct SurveyData {
    pub name: Option<String>,
    pub score: f64,
    pub votes: u64,
}

#[derive(Serialize, Deserialize)]
pub struct Survey {
    pub title: String,
    pub data: HashMap<String, SurveyData>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub password: Option<Vec<u8>>,
    #[serde(skip_serializing_if = "std::ops::Not::not")]
    #[serde(default)]
    pub is_closed: bool,
}
