#[macro_use]
extern crate rocket;

mod ballot_id;
mod forever;
mod password;
mod survey;
mod templates;

use std::collections::HashMap;
use std::io;
use std::mem;

use rocket::http::uri::Absolute;
use rocket::http::{Cookie, CookieJar, SameSite, Status};
use rocket::response::Redirect;

use rocket::form::Form;
use rocket::tokio::fs::File;
use rocket::tokio::io::AsyncReadExt;
use rocket::tokio::io::AsyncWriteExt;

use ballot_id::BallotId;
use password::{password_hash, password_verify, PasswordResult};
use survey::{Survey, SurveyData};

// In a real application, these would be retrieved dynamically from a config.
const HOST: Absolute<'static> = uri!("https://score.vote");
const ID_LENGTH: usize = 10;

#[post("/", data = "<survey>")]
async fn create(mut survey: Form<HashMap<String, String>>) -> Result<Redirect, Status> {
    let id = BallotId::new(ID_LENGTH);
    let mut title = "Survey".to_string();
    let mut password = String::new();
    let data: HashMap<String, SurveyData> = mem::replace(&mut *survey, HashMap::new())
        .into_iter()
        .flat_map(|(k, v)| {
            let k = k.trim().to_string();
            let v = v.trim().to_string();
            if k == "title" {
                title = v;
                return None;
            }
            if k == "password" {
                password = v;
                return None;
            }
            if v == "" {
                return None;
            }
            Some((
                k,
                SurveyData {
                    name: Some(v),
                    score: 0.0,
                    votes: 0,
                },
            ))
        })
        .collect();
    if data.len() < 2 {
        return Err(Status::BadRequest);
    }
    let mut file = File::create(id.file_path()).await.map_err(|_| Status::InternalServerError)?;
    let survey = Survey {
        title,
        data,
        password: if password == "" {
            None
        } else {
            Some(password_hash(&password))
        },
        is_closed: false,
    };
    file.write_all(serde_json::to_string(&survey).map_err(|_| Status::InternalServerError)?.as_bytes())
        .await
        .map_err(|_| Status::InternalServerError)?;
    Ok(Redirect::to(uri!(HOST, results(id)).to_string()))
}

#[get("/<id>")]
async fn voteform(id: BallotId<'_>, cookies: &CookieJar<'_>) -> Option<templates::VoteForm> {
    let mut file = File::open(id.file_path()).await.ok()?;
    let mut buf = String::new();
    file.read_to_string(&mut buf).await.ok()?;
    let survey: Survey = serde_json::from_str(&buf).ok()?;
    let mut cookie_set = false;
    if let Some(x) = cookies.get("X") {
        if x.value() == id.as_str() {
            cookie_set = true;
        }
    }
    let url = format!("{HOST}/{id}", id = id.as_str());
    Some(templates::VoteForm {
        title: survey.title,
        data: survey.data,
        cookie_set,
        url,
        is_closed: survey.is_closed,
        has_password: survey.password.is_some(),
    })
}

#[post("/<id>", data = "<input>")]
async fn vote(
    id: BallotId<'_>,
    mut input: Form<HashMap<String, Option<u64>>>,
    cookies: &CookieJar<'_>,
) -> io::Result<Redirect> {
    let mut file = File::open(id.file_path()).await?;
    let mut buf = String::new();
    file.read_to_string(&mut buf).await?;
    let mut survey: Survey = serde_json::from_str(&buf)?;
    for (k, v) in mem::replace(&mut *input, HashMap::new()).into_iter() {
        if let Some(v) = v {
            if let Some(n) = survey.data.get_mut(&k) {
                if v > 99 {
                    return Err(io::Error::new(io::ErrorKind::Other, "vote overrun"));
                }
                let mut partial = n.score * (n.votes as f64);
                n.votes += 1;
                partial += v as f64;
                n.score = partial / (n.votes as f64);
            }
        }
    }
    if let Some(x) = cookies.get("X") {
        if x.value() == id.as_str() {
            return Err(io::Error::new(io::ErrorKind::Other, "voted twice"));
        }
    }
    if survey.is_closed {
        return Err(io::Error::new(io::ErrorKind::Other, "voted twice"));
    }
    let cookie = Cookie::build("X", id.as_str().to_string())
        .path("/")
        .permanent()
        .same_site(SameSite::None)
        .finish();
    cookies.add(cookie);
    mem::drop(file);
    let mut file = File::create(id.file_path()).await?;
    file.write_all(serde_json::to_string(&survey)?.as_bytes())
        .await?;
    Ok(Redirect::to(uri!(HOST, results(id)).to_string()))
}

#[get("/<id>/results")]
async fn results(id: BallotId<'_>) -> Option<templates::Results> {
    let mut file = File::open(id.file_path()).await.ok()?;
    let mut buf = String::new();
    file.read_to_string(&mut buf).await.ok()?;
    let survey: Survey = serde_json::from_str(&buf).ok()?;
    let url = format!("{HOST}/{id}", id = id.as_str());
    let mut data = survey.data.into_iter().collect::<Vec<_>>();
    data.sort_by_key(|d| (float_ord::FloatOrd(-d.1.score), !d.1.votes));
    Some(templates::Results {
        title: survey.title,
        data,
        url,
        is_closed: survey.is_closed,
        has_password: survey.password.is_some(),
        bad_password: false,
    })
}

#[derive(FromForm)]
struct PasswordForm {
    password: String,
}

#[post("/<id>/close", data="<password_form>")]
async fn close(id: BallotId<'_>, password_form: Form<PasswordForm>) -> Option<templates::Results> {
    let mut file = File::open(id.file_path()).await.ok()?;
    let mut buf = String::new();
    file.read_to_string(&mut buf).await.ok()?;
    let mut survey: Survey = serde_json::from_str(&buf).ok()?;
    mem::drop(file);
    let url = format!("{HOST}/{id}", id = id.as_str());
    let bad_password = if let Some(ref mut password_hash) = survey.password.clone() {
        if password_verify(&password_form.password, password_hash) == PasswordResult::Passed {
            survey.is_closed = true;
            let mut file = File::create(id.file_path()).await.ok()?;
            file.write_all(serde_json::to_string(&survey).ok()?.as_bytes())
                .await.ok()?;
            false
        } else {
            true
        }
    } else {
        true
    };
    let mut data = survey.data.into_iter().collect::<Vec<_>>();
    data.sort_by_key(|d| (float_ord::FloatOrd(-d.1.score), !d.1.votes));
    Some(templates::Results {
        title: survey.title,
        data,
        url,
        is_closed: survey.is_closed,
        has_password: survey.password.is_some(),
        bad_password,
    })
}

#[get("/")]
fn index() -> forever::CacheForever<templates::Index> {
    forever::CacheForever(templates::Index)
}

#[get("/robots.txt")]
fn robots_txt() -> forever::CacheForever<&'static str> {
    forever::CacheForever("User-agent: *\nDisallow: /\n")
}

#[launch]
fn rocket() -> _ {
    rocket::build().mount(
        "/",
        routes![index, create, results, voteform, vote, close, robots_txt],
    )
}
