A standalone polling application.

sample ballot https://score.vote/iFpiN6jSts

Recommended systemd setup:

```text
[Unit]
Description=score-vote
After=network.target auditd.service
Before=nginx.service

[Service]
ExecStart=/home/notriddle/score.vote/target/release/score-vote
Restart=on-failure
User=notriddle
Group=notriddle
WorkingDirectory=/home/notriddle/score-vote
Env=CARGO_MANIFEST_DIR=/home/notriddle/score-vote

[Install]
WantedBy=multi-user.target
```

## Copyright

This software may be used under the terms of either:

* The MIT License, as show in LICENSE-MIT
* The Apache 2.0 license, as show in LICENSE-APACHE

Any merge requests made against the repository are assumed to be usable under these terms.
