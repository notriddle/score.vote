use std::env;
use std::error::Error;
use std::fs::File;
use std::io::{self, Write};
use std::mem;
use std::process::Command;
fn main() -> Result<(), Box<dyn Error>> {
	let commit = {
		let commit = Command::new("git")
			.arg("rev-parse")
			.arg("HEAD")
			.output()?;
		if !commit.status.success() {
			println!("stdout = {:?}", commit.stdout);
			println!("stderr = {:?}", commit.stderr);
			return Err(io::Error::new(io::ErrorKind::Other, "failed to get commit").into());
		}
		if let Ok(commit) = String::from_utf8(commit.stdout) {
			commit.trim().to_string()
		} else {
			println!("stderr = {:?}", commit.stderr);
			return Err(io::Error::new(io::ErrorKind::Other, "commit invalid UTF-8").into());
		}
	};
	let mut f = File::create(format!("{}/head", env::var("OUT_DIR")?))?;
	f.write_all(commit.as_bytes())?;
	mem::drop(f);
	println!("cargo:rerun-if-changed=.git");
	Ok(())
}
